public class Variables {


    public static void main(String[] args) {

        int favoriteNum = 21;
        System.out.println(favoriteNum);

        String myName = "Victor";
//        System.out.println(myName);

//        long myNum= 123l; //long is for whole numbers. Type number and add 'l' behind it
//        System.out.println(myNum);

//        float myNum = 3.14; wrong way of doing it
//        System.out.println(myNum);

        float myNum = 3.14f; // casting it as a float
//        double myNum = 3.14;
//        System.out.println(myNum);
//        int x = 10;
//        System.out.println(x++); //10
//        System.out.println(x); //11

//        int y = 10;
//        System.out.println(++y); //11
//        System.out.println(y); // 11

//        String class; cant use class as a variable name

        String theNumberEight = "eight";
        System.out.println(theNumberEight);

        Object o = theNumberEight;
//        int eight = (int) o; //cant do this

        //shorthand assignment operators use google

//        int x = 5;
//        // x = x + 6;
//        x+=6; //shorthand
//        System.out.println(x); // 5 + 6 is 11

        int x = 7;
        int y = 8;
//        y = y * x;
        y*=x;
        System.out.println(y); //56

        int a = 20;
        int b = 2;
//        a = a / b;
        a/=b;
        System.out.println(a);


        //max value checkout slide 11

        int z = Integer.MAX_VALUE;
        System.out.println(z); //2147483647

        int zz = Integer.MIN_VALUE;
        System.out.println(zz); // -2147483648

        int zzz = Integer.MAX_VALUE + 1;
        System.out.println(zzz);




    }



}
