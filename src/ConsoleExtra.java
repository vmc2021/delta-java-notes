import java.util.Scanner;

public class ConsoleExtra {
    public static void main(String[] args) {

        //challenge #1
        System.out.println("Hello\nDelta Cohort");

        //challenge #2
        System.out.println("The sum of 1 plus 2 is " + (1+2));

//        int num1 = 10;
//        int num2 =20;
//        System.out.println(num1 + num2);

        //challenge #3
        Scanner scanner = new Scanner(System.in);//add a scanner to enter inputs
//        System.out.println("Please enter a number");
//        int number1 = scanner.nextInt();//class: scanner
//        System.out.println("Please enter a second number");
//        int number2 = scanner.nextInt();

        //using concatenation
//        System.out.println(number1 + "x" + number2 + "=" + (number1 * number2));

        //format specifiers
//        System.out.printf("%d x %d = %d ", number1, number2, number1 * number2);

        //challenge #4
//        System.out.println("Please enter a number");
//        double number1 = scanner.nextDouble();
//        System.out.println("Please enter a second number");
//        double number2 = scanner.nextDouble();
//        //the sum

//        System.out.printf("%.2f + %.2f = %.2f%n", number1, number2, number1 + number2);
//        System.out.printf("%.2f - %.2f = %.2f%n", number1, number2, number1 - number2);
//        System.out.printf("%.2f * %.2f = %.2f%n", number1, number2, number1 * number2);





        //challenge #5
//        System.out.println("Enter a string");
//        String userInput = scanner.nextLine();
//        String str1= new String("pneumonoultramicroscopicsilicovolcanoconiosis");
//        System.out.println("Your string has this many characters: " + str1.length());



        //challenge#6
        System.out.println("Enter the first number");
        //first input
        int num1 = scanner.nextInt();
        System.out.println("Enter second number");
        //second input
        int num2 = scanner.nextInt();
        System.out.println("Enter third number: ");
        //third input
        int num3 = scanner.nextInt();
        //print average
        System.out.printf("The average is: %d ", (num1+num2+num3)/3);




    }

// Console Extra
/*
1. Write a Java program to print "Hello" in the console and then print your name on a separate line
2. Write a Java program that will print the sum of two numbers
3. Write a Java program that takes two numbers as input and display the product of two numbers
Test Data:
Input first number: 25
Input second number: 5
Expected Output:
25 x 5 = 125
4. Write a Java program to print the sum, multiply, subtract, divide, and remainder of two numbers
Test Data:
Input first number: 125
Input second number: 24
Expected Output :
125 + 24 = 149
125 - 24 = 101
125 x 24 = 3000
125 / 24 = 5
125 % 24 = 5
5. Write a Java program that will take a string as an input and display the number of characters of that string
Test Data:
Input string: pneumonoultramicroscopicsilicovolcanoconiosis
Expected Output:
Your string contains 45 characters.
6. Write a Java program that takes three numbers as input to calculate and print the average of the numbers.
*/




}
