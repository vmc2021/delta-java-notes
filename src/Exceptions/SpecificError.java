package Exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SpecificError {

    public static void main(String[] args) {
        int choice = 0;
        Scanner scanner = new Scanner(System.in);

            int[] numbers = {10, 11, 12, 14, 15};
        System.out.println("Please enter the index of the array: ");





        try{
            choice = scanner.nextInt();
            System.out.printf("numbers[%d] = %d%n", choice, numbers[choice]);

        }
        catch (ArrayIndexOutOfBoundsException e1){
            System.out.println("Error: Index is invalid");
        }
        catch (InputMismatchException e){
            System.out.println("Error: You did not enter an integer.");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //enter: 10, error index is invalid

        //enter: Hello, Input Mismatch Exception





    }//end of psvm

     //Bonus Jose's solution
    public static void getBinary(String userInput1) {
        String bin = userInput1;
        int num1 = Integer.valueOf(bin, 2);
        System.out.println("Your number is: " + num1);
    }
//    public static void getHexadecimal(String userInput2) {
//        int num2 = Integer.valueOf();
//        System.out.println("Your number is: " + num2);
//    }

    //stephens solution




}//end of class
