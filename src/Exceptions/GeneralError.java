package Exceptions;

import java.util.Scanner;

public class GeneralError {

    public static void main(String[] args) {
        int numerator;
        int denominator;
        Scanner scanner = new Scanner(System.in);



        try{

            System.out.println("Enter the numerator: ");
            numerator = scanner.nextInt();

            System.out.println("Enter the denominator");
            denominator = scanner.nextInt();

            System.out.println( "The result is: " + numerator / denominator);

        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        finally{
            System.out.println("---End of Error Handling Example---");
        }

    }
}
