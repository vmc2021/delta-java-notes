import java.util.Arrays;

public class ArraysExercises {

    public static void main(String[] args) {
        //main 1====================
        int[] numbers = {3, 1, 5, 2, 4};

        System.out.println(numbers);// error

//            implicit call to 'toString()'
        System.out.println(Arrays.toString(numbers));// [3, 1, 5, 2, 4]


        //main 2================create second array

        //part 1:
        int[] numbers2 = Arrays.copyOf(numbers, numbers.length);

        //part 2: print out i numerical order
        Arrays.sort(numbers2);

        System.out.println(Arrays.toString(numbers2));




        //main 3 ================ add a number to the numbers array

//        int[] numbers = {3, 1, 5, 2, 4, 6};
//        System.out.println(Arrays.toString(numbers));

        int[] moreNumbers = addNumber(numbers, 7);
        Arrays.sort(moreNumbers);
        System.out.println(Arrays.toString(moreNumbers));// returns 7


        //main 4 ================  create an array that holds 3 strings

        //instance means:
        // something[0] = "name"
        // something[1] = "name"


        String[] persons = new String[3];
        persons[0] = "Alyssa";
        persons[1] = "Angela";
        persons[2] = "Jose";

        // for loop solution
//        for (int i = 0; i < persons.length; i++) {
//            System.out.println(persons);
//        }

        //enhanced for loop
        //person is like a variable
        for (String person : persons) {
            System.out.println(person);
        }
//========================================================================
//     new string to an array
//        String[] morePeople = addToArray(, "Josh");
//        System.out.println(Arrays.toString(morePeople)); // returns josh at the end

//===============================================================================
        //create an array of 3 integers/last exercise
//        int[] digits = {1, 2, 3};
//        System.out.println(sumOfArray(digits));

//============================================================================

//        String[] greeting = {"hello", }
//


    }//end of main method

 //===================================================================================================
 //===================================================================================================
 //====================================================================================================



///main 3   ==================================================

    public static int[] addNumber(int[] numberArray, int number) {

    int[] numberCopy = Arrays.copyOf(numberArray, numberArray.length + 1);

    numberCopy[numberCopy.length - 1] = number;

    return numberCopy;
    }
//================================================================================

    //create a static method named addToArray

    //step 1:
    public static  String[] addToArray(String[] persons, String person) {
        String[] personCopy = Arrays.copyOf(persons, persons.length + 1);
        personCopy[personCopy.length - 1] = person;
        //return
        return personCopy;

        //step 2: got to main method

    }
    //==================================================================================================
// create a method that will take in an an array asa parameter
    public static  int sumOfArray(int[] nums) {
        return (nums[0] + nums[1] + nums[2]);
    }

//====================Last challenge==============================================
    public  static String stringTogether(String[] str) {

        return str[0] + str[1] + str[2];
    }

}//end of class
