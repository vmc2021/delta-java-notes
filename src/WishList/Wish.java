package WishList;

public class Wish {

    //fields
    private String item;
    private double price;


    //constructor

    public Wish(String item, double price) {

        this.item = item;
        this.price = price;
    }


    //getters/setter


    public String getName() {
        return item;
    }

    public void setName(String name) {
        this.item = item;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
