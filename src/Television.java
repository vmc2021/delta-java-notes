public class Television {

    private String title;
    private int year;
    private String studio;


    //empty constructor
    public Television() {
        System.out.println("The first show I ever binge watched: Breaking Bad ");
    }

    //constructor
    public Television(String title, String studio, int year) {

        this.title = title;
        this.year = year;
        this.studio = studio;



    }



    public void printMessage() {
        System.out.println("the first show I binge watched");
    }
 //================================================================
    //field/instance method

    public String displayInfo() {
        return String.format("%s came out in %d, and was produced by %s", title, year, studio);
    }


//================getter/ setter methods================

      //get
    public String getTitle() {
            return title;
    }
    //set
    public void setTitle(String title) {
            this.title = title;
    }

    //get
    public  int getYear() {
        return year;
    }
    //set
    public void setYear(int year) {
        this.year = year;
    }
    //get
    public String getStudio() {
        return studio;
    }
    //set
    public void setStudio(String studio) {
        this.studio = studio;
    }














}
