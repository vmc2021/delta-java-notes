public class Student {
    //fields
    //access modifiers public and private
    public String name;
    public String cohort;
    //private
    private double grade;





    //constructor always has the same name as the class
    //THREE WAYS OF CREATING A CONSTRUCTOR
    public Student(String studentName, String assignedCohort) {
        name = studentName;
        cohort = assignedCohort;
    }
    public Student(String studentName) {
        name = studentName;
        cohort = "Unassigned to a Cohort";
    }
    public Student(String name, String cohort, double grade) {
        //this keyword provides us away to refer to the Current instance
        this.name = name;
        this.cohort = cohort;
        this.grade = grade;
    }


    //getStudentInfo()
    public String getStudentInfo() {
        return String.format("Student Name: %s\nCohort Assigned: %s\n", name, cohort);
    }

    //sayHello()
    public  String sayHello() {
        return "Hello I am " + this.name;
    }

    //shareGrades()
    public  double shareGrade() {
        //how to access private variables
        return grade;
    }







}
