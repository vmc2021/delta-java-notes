package AbstractAndInterface;

//Notes:
//Abstract Class - a special type of class that is created  strickly to be  a 'base class'
// for other classes to derive from
// - cannot be instantiated
// - may have fields and method just like classes
// - they have Abstract Methods
//    - ABSTRACT METHODS - methods that has no body
//                   -only exists
//                     - doesn't contain a body


// how to make an abstract class?
// - use the 'abstract' keyword INSIDE OF THE CLASS DEFINITION


//GENERAL CONTRACT FOR A WRESTLER

//add key word between 'public' and 'class'
public abstract class Wrestler {

    //basic generic wrestler contract for Ric Flair


        //METHOD
        public  static void paymentForPerformance(int hours) {
            System.out.println("The wrestlers pay for tonight: " + 250*hours);
        }

    //abstract Methods. how two ways to create? ===============================================================

    //first way
    abstract public  void wrestlerName();

    //second way
    public abstract void themeMusic();

    public abstract void finisherMove();


}
