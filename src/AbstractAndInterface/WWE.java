package AbstractAndInterface;

//INTERFACE- much like abstract class in the way they cannot be INSTANTIATED
// INSTEAD, they must be implimented by classes or extended by other interfaces
//****

//contract for Steve Austin

public interface WWE {

    //abstract methods

    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisherMove();

    public abstract void paymentForPerformance(int hours);


}
