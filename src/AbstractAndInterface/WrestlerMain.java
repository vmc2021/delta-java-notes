package AbstractAndInterface;

public class WrestlerMain {

    public static void main(String[] args) {
        //create objects

        Wrestler wrestler1 = new RicFlair();


        WWE wrestler2 = new SteveAustin();


        wrestler1.wrestlerName();// returns Ric Flair
        wrestler1.finisherMove();
        wrestler1.paymentForPerformance(5);

        wrestler2.wrestlerName();
        wrestler2.finisherMove();
        wrestler2.paymentForPerformance(5);

    }


}
