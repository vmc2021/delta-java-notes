package AbstractAndInterface;

public class RicFlair extends Wrestler {


    //imported methods from wrestler
    @Override
    public void wrestlerName() {
        System.out.println("Ric Flair");
    }

    @Override
    public void themeMusic() {
        System.out.println("Wooo!");
    }

    @Override
    public void finisherMove() {
        System.out.println("Figure Four Leg Lock");
    }
    //hes going to follow the wrestler contract



}
