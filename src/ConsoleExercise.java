import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {
        double pi = 3.14159;
        System.out.printf("The value of pi is approximately %1.2f%n", pi);// pass in pi

//        EXPLORE THE SCANNER ClassPrompt

        //ENTER AN INTEGER
        Scanner scanner = new Scanner(System.in); //import scanner class
//        System.out.println("Enter an Integer: ");// add prompt
//        int userInt = scanner.nextInt();//data type: int , method: nextInt()
//        System.out.println("The integer output:" + userInt);


        //Prompt user to enter three words:

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Please enter three words, hit enter after each entry");
//        String userInput = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + userInput);
//
//        System.out.println("Enter second word: ");
//        String userInput2 = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + userInput2);
//
//        System.out.println("Enter third word: ");
//        String userInput3 = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + userInput3);


//        Second answer:

//        System.out.println("Please enter three words, hit enter after each entry");
//        String userInput1 = scanner.nextLine();
//        String userInput2 = scanner.nextLine();
//        String userInput3 = scanner.nextLine();
//        System.out.println("You entered: " + userInput1 + "\n" + userInput2 + "\n" + userInput3 );




//        //Prompt user to enter a sentence
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Please enter a sentence: ");
//        String line = scanner.next(); // scanner next() method goes here
//        System.out.println(line);

//        System.out.println("Please enter a sentence: ");
//        String newLine = scanner.nextLine();
//        System.out.println("Thank you, you entered: \n" + newLine);


        //CODEBOUND CLASSROOM

        System.out.println("Please enter the length in feet: ");
        double cLength = scanner.nextDouble();

        System.out.println("Enter the width: ");
        double cWidth = scanner.nextDouble();

        System.out.println("The perimeter is " + (2*cLength + 2*cWidth));

        System.out.println("the area is " + (cWidth * cLength));

            //bonus
            // volume of class room

        System.out.println("Enter the volume: ");
        double h = scanner.nextDouble();
        System.out.println("The volume of the class is " + (h * cLength * cWidth));



























    }
}
