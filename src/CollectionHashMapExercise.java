import java.util.HashMap;
import java.util.Map;


public class CollectionHashMapExercise {

    public static void main(String[] args) {



        HashMap<String, String> usernames = new HashMap<>();
        //append a specified element
        usernames.put("Alice", " alice001");
        usernames.put("Karen", " karen247");
        usernames.put("Juan", " juan2222");
        usernames.put("Joe", " joe21");
        System.out.println(usernames);




        //iterate through all the elements:
        System.out.println("Entries");
      for (String u : usernames.keySet()) {
          System.out.println(u + ":" + usernames.get(u));
      }

        //a.  test if hash map is empty or not
            boolean result = usernames.isEmpty();




        //b. get value of specified key in a map
        System.out.println(usernames.get("Joe"));
        String value = usernames.get("Joe");
        System.out.println("The value for Joe is: " + value);



        //c. test mapping for specified key
        System.out.println(usernames.containsKey("Juan") ? "Juan is in the list:" : "Juan is not in the list");



        //d. test mapping for specified value

        System.out.println(usernames.containsValue("alice001") ? "alice001 is on the list " : "alice001 is not on the list");


        //e. get set view of the keys in a map



        //f. get a collection view of the values contained in a map




    }






}
