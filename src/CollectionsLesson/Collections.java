package CollectionsLesson;


//imported by typing arraylist inside our class
import java.util.ArrayList;
import java.util.HashMap;

//import Collections


public class Collections {

    public static void main(String[] args) {
        //ArrayList and its Methods

        // 1. Initialize an ArrayList of Strings,call roasts
        // Add a light, medium, dark to the array list, one at a time


        //call our array list
//    ArrayList<String> make it a string. Each element is a string. name is roast. Its an object of ArrayList.
//    ArrayList<String> roasts = new ArrayList<>();
        //add
        ArrayList<String> roasts = new ArrayList<>();

        roasts.add("light"); //similar to
        roasts.add("medium");
        roasts.add("medium");
        roasts.add("dark");

        System.out.println(roasts);

        //2. Check to see if the list contains "dark",

        boolean result;

        result = roasts.contains("dark");
        //use terenary operator
        System.out.println(result ? "It does contain dark!" : "Does not contain dark");
        //does it contain expresso?
        result = roasts.contains("espresso");//we know we  dont have espresso
        //use terenary operator
        System.out.println(result ? "It does contain espresso!" : "Does not contain espresso");

        //3. Find the last index of "
        int index = roasts.lastIndexOf("medium");
        System.out.println("The last index of 'medium' is :" + index);// index of 2

        // 4. Check if the array list is empty
        if (roasts.isEmpty()) {
            System.out.println("The list is empty");
        }
        else {
            System.out.println("The list is not empty");
        }
        //convert to ternary operator list isEmpty
        System.out.println(roasts.isEmpty() ? "List is empty" : "List is Not empty");

        //5. Assign the array list an empty ArrayList object,
        roasts = new ArrayList<>();
        //and then check if its empty
        System.out.println(roasts.isEmpty() ? "List is empty" : "List is not Empty");

        //6. Put data back in our ro
        roasts.add("light");
        roasts.add("light");
        roasts.add("medium");
        roasts.add("medium");
        roasts.add("dark");
        roasts.add("espresso");
        System.out.println(roasts);

        //7. remove the espresso
        roasts.remove("espresso"); // remove string value
        System.out.println("Roasts now looks like: " + roasts);//espresso is removed from list

        //if we want to remove every instance of "light"

        boolean keepGoing = roasts.contains("light");

        //while keepGoing is true
        while (keepGoing) {
            if (roasts.contains("light")) {
                roasts.remove("light");
            }
            else {
                keepGoing = false;
            }
        }
        System.out.println("After out 'light' removal, roast: " + roasts);// left with [medium, medium, dark]

        //8. Remove  the element at
        roasts.remove(2);
        System.out.println("After removing index 2, roasts = " + roasts);// [medium, medium]

        //Create an ArrayLIst
        ArrayList<Integer> myNumbers = new ArrayList<>();
        myNumbers.add(23);
        myNumbers.add(50);
        myNumbers.add(32);
        myNumbers.add(91);

        //edit an element setting integer index: 3 to 100
        myNumbers.set(3,100);
        System.out.println(myNumbers);//[23, 50, 32, 100]

        //reordering list
        java.util.Collections.sort(myNumbers);
        System.out.println(myNumbers);//[23,32, 50, 100]

        //reverse order
        java.util.Collections.reverse(myNumbers);
        System.out.println(myNumbers);//[100, 50, 32, 23]


        //======================================================================================

        //HashMaps and their Methods

        //1. Create a HashMap named user names that contains:
        //a. first name / String
        //b. username/ String

        HashMap<String, String> usernames = new HashMap<>();

        // put some data in the hashmap
        usernames.put("Stephen", "stephen001");
        usernames.put("Karen", "beingkaren247");
        usernames.put("Juan", "theony1");
        usernames.put("Leslie", "sleepy21");

        System.out.println(usernames);//{Karen=beingkaren247, Juan=theony1, Leslie=sleepy21, Stephen=stephen001}


        // 2. re-initialize the HashMap using the .clear() method
        usernames.clear();
        System.out.println(usernames); // {}

        // 3. use the .put() method to add "AJ" -> "fridaynext" back to the map
        usernames.put("AJ", "fridaynext");
        System.out.println(usernames);//{AJ=fridaynext}

        // 4. use the .putIfAbsent() method
        //  key/value
        usernames.putIfAbsent("Stephen", "stephenguedea");
        usernames.putIfAbsent("Angela", "angelafjgjgj");
        System.out.println(usernames);//{AJ=fridaynext, Angela=angelafjgjgj, Stephen=stephenguedea}

        // 5. What happened with the .putIfAbsent()? Did both items get add? Why/ why not?
        // object key/ object value
        usernames.putIfAbsent("AJ", "allenjustin");
        System.out.println(usernames);

        //6. use the .remove() method to remove "Stephen"
        usernames.remove("Stephen");
        System.out.println(usernames);//{AJ=fridaynext, Angela=angelafjgjgj}

        //7. use the replace() method to change AJ's username to ablanco
        usernames.replace("AJ", "ablanco");
        System.out.println(usernames);//{AJ=ablanco, Angela=angelafjgjgj}

        // Change the key for "AJ" (have to completely remove and re-add that element to the HashMap)
        String ajValue = usernames.get("AJ");

        usernames.remove("AJ");
        usernames.put("Allen Justin", ajValue);
        System.out.println("After changing 'AJ' key: " + usernames);//{Allen Justin=ablanco, Angela=angelafjgjgj}


        // 8. Use the .clear() method to clear the map

        usernames.clear();

        // 9. Use the .isEmpty() method to verify that it was cleared

        System.out.println(usernames.isEmpty() ? "HashMap is Empty" : "HashMap is not empty");//HashMap is Empty







    }// end of psvm



}
