package MethodsReview;

import java.util.Locale;

public class MethodLecture {

    //whats a method definition? public static returnType name

    public static int printNumber() {
        return 1;
    }

    public static void sayHello() {
        System.out.println("Hello");
    }

    public static long printFive() {
        return 5;//
    }

    public static  String yell(String str) {
        return str.toUpperCase() + "!!!";
    }

    //method overloading. same method, but with different sets of parameters

    //version 1
    public static String yell() {
        return  "I don't know what we're yelling about!";
    }
    //version 2
    public static String yell(String str, String name) {
        return str.toUpperCase() + "" + name + "!!!";
    }






}
