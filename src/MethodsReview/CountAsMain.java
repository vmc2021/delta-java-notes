package MethodsReview;

import java.util.Scanner;

public class CountAsMain {


    public static void main(String[] args) {

        CountAs count = new CountAs();//object count
        //other possible objects
        CountAs count2 = new CountAs();//object count2
        CountAs count3 = new CountAs();//object count3


        Scanner scanner = new Scanner(System.in);
        System.out.println("======");
        System.out.println("Enter a string: ");

        //new variable
        String userInput = scanner.nextLine().toLowerCase();

        System.out.println("You entered " + userInput);
        System.out.println("Number of A's = " + count.countingTheA(userInput));// need count to access the countingTheA method
        System.out.println("==========");




    }



}
