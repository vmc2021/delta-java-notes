package MethodsReview;

import java.util.Scanner;

public class CountAs {
    public static int countingTheA(String str) {

        //Java method to count all the A's in a string.

        //local variable
        int counter = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'a') {
                counter++;
            }
        }
        //under for loop
        return counter;


    }

    public static void main(String[] args) {
        //add SOUT statement
        System.out.println(countingTheA("alalalala"));

        //use the scanner class
        Scanner scanner = new Scanner(System.in);
        System.out.println("======");
        System.out.println("Enter a string: ");

        //new variable
        String userInput = scanner.nextLine().toLowerCase();

        System.out.println("You entered " + userInput);
        System.out.println("Number of A's = " + countingTheA(userInput));
        System.out.println("==========");
    }

}
