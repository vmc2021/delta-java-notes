package MethodsReview;

public class MethodLectureTest {
    public static void main(String[] args) {
        //create an object our MethodLecture class
        //"instantiating an object"
        //ClassName objectName = new ClassName(arguments);

        //WE NEED TO INSTANTIATE THE OBJECT OF THE CLASS NAME: class MethodLecture
        MethodLecture m1 = new MethodLecture();
        System.out.println(m1.printNumber());//


        m1.sayHello();

        System.out.println(m1.printFive());
        System.out.println(m1.yell("message"));



    }
}
