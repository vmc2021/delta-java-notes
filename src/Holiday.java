public class Holiday {

    //variables
    private String name;
    private int day;
    private String month;

    //constructor
    public Holiday (String name, int day, String month) {
        this.name = name;
        this.day = day;
        this.month = month;
    }

    public String greeting() {
        return String.format("hello from %s %s");
    }




}
