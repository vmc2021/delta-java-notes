import java.util.Scanner;

public class TelevisionMain {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        //objects 1,2,3,4
        Television television1 = new Television("The Office","NBC", 2005);
        Television television2 = new Television("Weeds", "Showtime", 2005 );
        Television television3 = new Television("Breaking Bad", "AMC", 2008);

        //prompt user to enter a show
//        System.out.println("Enter the name of your tv show");
//        String input;
//        input = scanner.nextLine();
//
//        //prompt user to name the studio
//        System.out.println("Enter the name of the studio");
//        String input2 = scanner.nextLine();
//
//        //prompt the year show first aired
//        System.out.println("Enter the year show aired");
//        int input3 = scanner.nextInt();



//========ACCESS GETTER/SETTER========================================================================
        //solution1
//        System.out.println(television3.getTitle() + " came out in " + television3.getYear());
//        //solution 2
//        System.out.println(television1.displayInfo());


//=======================================================================================
        //access getter/setter

        //object 4
//        Television television4 = new Television();
//        int theYear = television4.getYear();
//        System.out.println("The year Breaking Bad first aired on was : " + theYear);


//=========bonus scanner====================================================

        //prompt user to enter a show title
        System.out.println("Enter show title");
        String showInput = scanner.nextLine();

        //prompt user to enter studio
        System.out.println("Enter studio name");
        String studioInput = scanner.nextLine();


        //prompt user to enter year
        System.out.println("Enter release year");
        int yearInput = scanner.nextInt();


        Television television4 = new Television(showInput, studioInput, yearInput);
        System.out.println(television4.displayInfo());



    }//end of main


}//end of class
