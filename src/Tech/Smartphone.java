package Tech;

public class Smartphone extends Computer {

    //fields
    double screenSize;


    public Smartphone(String b, String m, int p, double s) {
        super(b, m, p);
        this.screenSize = s;
    }


    //methods getter/setter


    public double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(double screenSize) {
        this.screenSize = screenSize;
    }
}
