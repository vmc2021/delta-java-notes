package Tech;

public class Laptop extends Computer {

    //fields
    double weight;


    //constructor
    public Laptop(String b, String m, int p, double w) {
        super(b, m, p);
        this.weight = w;
    }


    //methods getter/setter


    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
