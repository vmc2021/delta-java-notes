package Tech;

public class Computer {

    //Fields
protected String brand;
protected String model;
protected int price;




    //constructor
public Computer(String b, String m, int p) {
    this.brand = b;
    this.model = m;
    this.price = p;
}



    //methods


    public String getBrand() {
        return brand;
    }

    void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
