import java.util.Scanner;

public class ControlFlowExercise {

    public static void main(String[] args) {

        //WHILE LOOP

//        int i = 9;
//
//        while (i <= 23) {
//            System.out.printf("%d ", i);// jose's example
//            i++;
//        }



        //DO WHILE
//        int a = 0;
//        do {
//            System.out.println(a + "");
//            a = a + 2; // or x+=2
//        }while (a <= 100);

//        int y = 100;
//        do{
//            System.out.println(y);
//            y = y - 5;// or y-=5
//        }while (y >= -10);
//
//        long z = 2;// don't use integer. Cast long
//        do {
//            System.out.println(z);
//            z *= z;
//        }while (z < 1000000);


        //TABLE OF POWERS use do while
//        System.out.println("What number would you like to go up to? ");

        Scanner scanner = new Scanner(System.in);
//        String choice;
//        int userInt = scanner.nextInt();
//        System.out.println("");
//        System.out.println("Here is your table!");
//        System.out.println("");
//        System.out.println("number | squared | cubed");
//        System.out.println("------ | ------- | -----");
//        for (int x = 1; x <= userInt; x++) {
//            System.out.println(x + "      | " + (x * x) + "       |" + "  " + (x * x * x));
//        }
//        System.out.println("Continue? [y/n]");
//        choice = scanner.nextLine();
//        while (choice.equalsIgnoreCase("y"));
        //Stephen's solution:



        //CONVERTING NUMBER GRADES TO LETTER GRADES use do while
        System.out.print("Enter your numeric test score : ");
        int score;
        char grade;
        String typeChoice;

         score = scanner.nextInt();

         //start with a do statement

        // Calculate the grade.
        if (score >= 90)
        {
            grade = 'A';
        }
        else if (score >= 80)
        {
            grade = 'B';
        }
        else if (score >= 70)
        {
            grade = 'C';
        }
        else if (score >= 50)
        {
            grade = 'D';
        }
        else
        {
            grade = 'F';
        }

        // Display the grade.
        System.out.println("Your grade is " + grade);
        //end with a while statement
//        while (typeChoice.equalsIgnoreCase())
        System.out.println("Would you like to continue? [y/n]");



    }

}
