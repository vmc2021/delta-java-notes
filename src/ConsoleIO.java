import java.util.Scanner;

public class ConsoleIO {

    public static void main(String[] args) {

        //printing out data in our console
        String cohort = "Delta";
//        System.out.println(cohort);

        String greeting = "Bonjour";
//        System.out.println(greeting);

        //println vs print
//        System.out.print(cohort);
//        System.out.print(greeting);// print adds everything together

        System.out.println();
        // souf/printf / format
//        System.out.printf("%s %s!%n", greeting, cohort);
//        System.out.format("%s, %s!", greeting, cohort);
        //Format Specifiers

        double price1 = 23.459;
        double price2 = 50;
        double price3 = 5.40;
        double total = (price1 + price2 + price3);
//        System.out.println(total);
//        System.out.printf("Your total: $%7.2f%n", total);
//        System.out.printf("Your total: $%7.3f%n", total);
//        System.out.printf("Your total: $%7f%n", total);
//        //money amount, getting two decimals
//        System.out.printf("Your total: $%.2f%n", total);

        //Scanner Class-get input from the console
        Scanner scanner = new Scanner(System.in);

        //example: create a prompt getting a string
        System.out.println("Enter your name: ");// prompt user to enter data
        //create a variable that will obtain whatever the user enters
        String userInput = scanner.nextLine(); //obtaining the vale of the user input
        //display the data that the user entered
        System.out.println("Thank you, you entered: \n" + userInput);

        //scanner class to retrieve a number
        System.out.println("Enter an Integer: ");
        //variable
        int userInt = scanner.nextInt();
        //output
//        System.out.println("The integer you entered was " + userInt);

        System.out.printf("The integer you entered was %d", userInt);
        //Note: %d format specifier

        // for getting decimal numbers...
//        double userInt = scanner.nextDouble();
//        System.out.printf("The integer you entered was %.2f", userInt);




    }
}
