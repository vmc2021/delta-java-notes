package Booklist;

import java.util.ArrayList;
import java.util.Scanner;

public class BookListFeatures {

    //similar to Television Exercise

    //every object needs an author and title
    ArrayList<Book> bookList = new ArrayList<>();

    //method to add
    public void addingBook() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Title: ");
        String titleInput = scanner.nextLine();

        System.out.println("Enter Author: ");
        String authorInput = scanner.nextLine();

        //constructor
        Book newBook = new Book(authorInput, titleInput);

        //how to add 'newBook' to ArrayList 'bookList'?
        bookList.add(newBook);

        System.out.println("Book has been added successfully");


       displayList();



    }
    //method
    public void displayList() {
        for (Book book : bookList) {
            System.out.println(book.getAuthor() + " " + book.getTitle());
        }
    }


}
