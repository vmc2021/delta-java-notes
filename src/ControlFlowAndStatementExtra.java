import java.util.Scanner;

public class ControlFlowAndStatementExtra {
    public static void main(String[] args) {

        //Challenge 1: POSITIVE OR NEGATIVE

        Scanner s = new Scanner(System.in);

//        System.out.println("Please enter a number: ");
//
//        int input = s.nextInt();
//
//        if (input > 0)
//        {
//            System.out.println("Number is positive");
//        }
//        else if (input < 0)
//        {
//            System.out.println("Number is negative");
//        }
//        else
//        {
//            System.out.println("Number is zero");
//        }




        //Challenge 2: GREATEST NUMBER



//        int x, y, z;
//        System.out.print("Enter the first number:");
//        x = s.nextInt();
//        System.out.print("Enter the second number:");
//        y = s.nextInt();
//        System.out.print("Enter the third number:");
//        z = s.nextInt();
//        if(x > y && x > z)
//        {
//            System.out.println("Largest number is:"+x);
//        }
//        else if(y > z)
//        {
//            System.out.println("Largest number is:"+y);
//        }
//        else
//        {
//            System.out.println("Largest number is:"+z);
//        }


        //Challenge 3: NUMBER OF DAY

//        Scanner in = new Scanner(System.in);
//        System.out.print("Please enter a number 1-7 to correspond to the days of the week(sunday-saturday): ");
//        int day = in.nextInt();
//
//        System.out.println(getDayName(day));
//    }
//
////         Get the name for the Week
//    public static String getDayName(int day) {
//        String dayName = "";
//        switch (day) {
//            case 1: dayName = "Sunday"; break;
//            case 2: dayName = "Monday"; break;
//            case 3: dayName = "Tuesday"; break;
//            case 4: dayName = "Wednesday"; break;
//            case 5: dayName = "Thursday"; break;
//            case 6: dayName = "Friday"; break;
//            case 7: dayName = "Saturday"; break;
//            default:dayName = "Invalid day range";
//        }
//
//        return dayName;


        //Challenge 4: NUMBER OF DAYS IN A MONTH


//        int number_Of_DaysInMonth = 0;
//        String MonthOfName = "Unknown";
//        System.out.print("Input a month number: ");
//        int month = s.nextInt();
//        switch (month) {
//            case 1:
//                MonthOfName = "January";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 2:
//                MonthOfName = "February";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 3:
//                MonthOfName = "March";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 4:
//                MonthOfName = "April";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 5:
//                MonthOfName = "May";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 6:
//                MonthOfName = "June";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 7:
//                MonthOfName = "July";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 8:
//                MonthOfName = "August";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 9:
//                MonthOfName = "September";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 10:
//                MonthOfName = "October";
//                number_Of_DaysInMonth = 31;
//                break;
//            case 11:
//                MonthOfName = "November";
//                number_Of_DaysInMonth = 30;
//                break;
//            case 12:
//                MonthOfName = "December";
//                number_Of_DaysInMonth = 31;
//        }
//        System.out.print(MonthOfName + "  has " + number_Of_DaysInMonth + " days\n");




        // Challenge 5: THREE NUMBERS


        Scanner in = new Scanner(System.in);

        System.out.print("Input first number: ");
        int a = in.nextInt();
        System.out.print("Input second number: ");
        int b = in.nextInt();
        System.out.print("Input third number: ");
        int c = in.nextInt();

        if (a == b && a == c)
        {
            System.out.println("All numbers are equal");
        }

        else if ((a == b) || (a == c) || (c == b))
        {
            System.out.println("Neither all are equal or different");
        }
        else
        {
            System.out.println("All numbers are different");
        }

    }


}

