import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class StringMethodsExercise {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

//        int myLength = "Good afternoon, good, evening, and good night";
        //challenge 1:
        String myLength = "Good afternoon, good evening, and good night";
        System.out.println("There are " +myLength.length()+ " characters"); // returns 44

        //challenge 2:
        String uCase = "Good afternoon, good evening, and good night";
        System.out.println(uCase.toUpperCase());// returns GOOD AFTERNOON....

        String lCase = "Good afternoon, good evening, and good night";
        System.out.println(uCase.toLowerCase());// returns good afternoon.....

        String firstSubstring = "Hello World".substring(10);
        System.out.println(firstSubstring); // returns "World on 6, "lo World" on 3, "d" on 10

        String message = new String("Good evening, how are you?");
        System.out.println(message.substring(0,12)); // returns "Good evening"

        //create a char variable
        String myChar = "San Antonio";
        System.out.println(myChar.charAt(0));// returns S
        System.out.println(myChar.charAt(4));// returns A
        System.out.println(myChar.charAt(7)); // returns o



        //====================================================================
        String alpha = "Alyssa Angela Jose Victor";

        String[] splitAlpha = alpha.split(" "); // take of comma here to add commas to the console

        System.out.println(Arrays.toString(splitAlpha));// add Arrays library and toString



        //===============================================================

        String m1 = "Hello,";
        String m2 = "how are you?";
        String m3 = "I love Java!";
        System.out.println(m1 + m2 + "\n" + m3);//use concatenation


        //===============================================================

        int results = 89;
        System.out.println("you" + results + "marks for your test!"); // use concatenation


        //=========================================================
        //bonus







    }
}
