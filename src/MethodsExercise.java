import javax.swing.*;
import java.util.Scanner;

public class MethodsExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // We call our methods here


//=======FIRST CHALLENGE==========================================
        //add method
        int sum = addNumbers(8, 4);
        System.out.println("The sum is: ");
        System.out.println(sum);

        //subtraction method
        int sub = subtractNumbers(8,4);
        System.out.println("The difference is: ");
        System.out.println(sub);

        //multiplication method
        int mul = multiplyNumbers(8,4);
        System.out.println("The product is: ");
        System.out.println(mul);

        //division method
        int div = divideNumbers(8,4);
        System.out.println("The quotient is: ");
        System.out.println(div);

        //modulus
        int mod = modNumbers(8,4);
        System.out.println("The modulus is: ");
        System.out.println(mod);

//======SECOND CHALLENGE: GET INTEGER====================================


//        Stephens solution getInteger
        getInteger(1,9);


//=======THIRD CHALLENGE: CALCULATE FACTORIAL===================================================
        //testing calFactorial
    calFactorial(calFactorial(5));

        //testing scannerFactorial()
//        scannerFactorial(scanner);

//=======AVERAGE OF THREE========================================================================
//ALYSSA'S SOLUTION
        System.out.println();
        double x = scanner.nextDouble();
        System.out.println();
        double y = scanner.nextDouble();
        System.out.println();
        double z = scanner.nextDouble();
        System.out.println("The Average value is" + average(x,y,z) + "\n");


//STEPHEN'S SOLUTION
        avg();




//=======BONUS:======================================================================================











    }//End of PSVM

//====FIRST CHALLENGE===========================================
    //addNumbers
    public static int addNumbers(int num1, int num2) {

        return num1 + num2;
    }
    //subtractNumbers
    public static int subtractNumbers(int num1, int num2){
        return num1 - num2;//return to use in application or sout to console log it
    }
    //multiplyNumbers
    public static  int multiplyNumbers(int num1, int num2) {

        return num1 * num2;
    }
    //divideNumbers
    public static int divideNumbers(int num1, int num2) {

        return num1 / num2;
    }
    public static int modNumbers(int num1, int num2) {

        return num1 % num2;
    }
    ///=========SECOND CHALLENGE: GET INTEGER============================================


//        Stephens solution getInteger
    public static int getInteger(int min, int max) {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Enter a valid number (%d - %d)%n", min, max);
        int userInput = scanner.nextInt();

        if (userInput >= min && userInput <= max) {
            System.out.println("valid number!");
            System.out.println("You entered: " + userInput);
            return userInput;
        }
        else {
            System.out.println("Invalid entry: Number must be between " + min + "" + max);
            return getInteger(min,max);
        }
    }





    //=========THIRD CHALLENGE:CALCULATE FACTORIAL=============================================

    public static long calFactorial(long num) {
        long output= 1;
        for (long i = 1; i <= num; i++) {
        output *= i;}
        return output;
    }

    //UPDATE OUR FACTORIAL TO BECOME USER INTERACTIVE



    //AVERAGE OF THREE NUMBERS
    //Alyssa's solution
    public static double average(double x, double y, double z)
    {
        return (x + y + z);
    }
    //STEPHEN'S SOLUTION
    public static void  avg() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter num1");
        double num1 = scanner.nextDouble();

        System.out.println("Enter num2 ");
        double num2 = scanner.nextDouble();

        System.out.println("Enter num3 ");
        double num3 = scanner.nextDouble();

        double result = (num1 + num2 + num3) / 3;
        System.out.println("Average: " + result );
    }




    //============BONUS:========================================================================







}