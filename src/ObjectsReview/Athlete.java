package ObjectsReview;

public class Athlete {
    //fields aka method or var - are var or methods that belongs to an object or a class
    //can be accessed with the  .operator
    // by default fields belongs to objects, but can be defined as belonging to a class with

    //variable FIELDS
    //non-static variables
    public String firstName;
    public String lastName;
    public int jerseyNumber;
    public String team;


    //non static method
    public  String sayHello() {
        return String.format("Hello from %s %s", firstName, lastName);// method sayHello
    }


}
