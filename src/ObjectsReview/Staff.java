package ObjectsReview;

public class Staff {

    //private/public/protected is access modifier = Visibility slide 15/16
    private String nameOfStaff;

    private final int hourlyRate = 30;//final doesn't allow us to change the value of 30

    private int hoursWorked;


    //===========================================================================

    //CONSTRUCTOR - A Method that is called when an object is created. can accept parameters and be overloaded.

    //empty constructor
    public  Staff () {
        System.out.println("Staff object being created...");
    }
    //constructor with String parameter
    public Staff(String name) {
        this.nameOfStaff = name;
        System.out.println("\n" + nameOfStaff);
        System.out.println("====================");
    }
    //constructor with two parameters
    public Staff(String firstName, String lastName) {
        this.nameOfStaff = firstName + " " + lastName;
        System.out.println("\n" + nameOfStaff);
        System.out.println("=======================");
    }


//======================================================================


    public void  printMessage() {
        System.out.println("Calculating Pay....");

    }
    public int calculatePay() {
        printMessage();

        int staffPay;
        staffPay = hoursWorked * hourlyRate;

        if (hoursWorked > 0) {
            return  staffPay;
        }
        else {
            return 0;
        }
    }



    //method overloading - same method name, different set of parameters

    public int calculatePay(int bonus, int allowance) {

        printMessage();

        if (hoursWorked > 0) {
            return hoursWorked * hourlyRate  + bonus + allowance;
        }
        else {
            return 0;
        }
    }

//getters/ setters Methods


    //setter
    public  void setHoursWorked(int hours) {

        if (hours > 0) {
            hoursWorked = hours;
        }
        else {
            System.out.println("Error: Hours cannot be smaller than zero");
            System.out.println("Error: HoursWorked is not updated");
        }
    }

    //getter
    public int getHoursWorked() {
        return hoursWorked;
    }






}
