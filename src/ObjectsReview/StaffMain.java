package ObjectsReview;

public class StaffMain {

    public static void main(String[] args) {

        //ClassName objectName = new ClassName(argument)
        Staff staff1 = new Staff();
        //constructor with one value
        Staff staff2 = new Staff("Jane");



        //access our setters methods from
        Staff staff = new Staff("Jane");
        staff2.setHoursWorked(160);
        int pay = staff2.calculatePay(1000, 400);
        System.out.println("Pay : $" + pay);

        //constructor with two values
        Staff staff3 = new Staff("Kevin", "Smith");
        staff3.setHoursWorked(160);
        int payStaff3 = staff3.calculatePay();
        System.out.println("Staff3 pay: $" + payStaff3);

    }

}
