package ExceptionsAndErrorHandling;

public class ExceptionsLesson {

    /*
    NOTES:
    EXCEPTIONS / ERROR HANDLING
    -ALLOW US TO
    -there are

    An exception is an unexpected event that occurs during a program's execution.
    -it affects the flow of our program, can cause the program to terminate

    An exception can occur for many reasons:
    Invalid user input
    Device failure
    Loss of network connection
    Physical limitations (disk memory capacity)
    Code Errors
    Opening an unavailable file

    Try-catch-finally block

    SYNTAX:
    try{
        do something
        }
    catch(type of error){
        do something regardless
        }
    finally{
        kfjfjfj
        }
     */


    public static void main(String[] args) {

//        try {
//            int divideByZero = 6/2;
//            System.out.println("Code in try block executed successfully");
//        }
//        catch (ArithmeticException e){
//            System.out.println("ArithmeticException: " + e.getMessage());
//            // display the default built-in exception message for ArithmeticException
//        }

//        try {
//            int divideByZero = 5/0;
//            System.out.println("Code in try block executed successfully");
//        }
//        catch (Exception e){
//            System.out.println("Exception: " + e.getMessage());
//            // display the default built-in exception message for Exception
//        }
        //Note: we can do a class inside psvm
        //example of multiple catch blocks
        class ListOfNumbers {
            public int[] arrayOfNumbers = new int[10];

            public void writeList(){
                try {
                    arrayOfNumbers[10] = Integer.parseInt("11");// .parseInt converts string to integer
                    System.out.println("Try block code executed");
                }//we cant have a try block by itself
                catch (NumberFormatException e1){
                    System.out.println("NumberFormatException: " + e1.getMessage());
                }
                catch (IndexOutOfBoundsException e2){
                    System.out.println("IndexOUtOfBoundsException: " + e2.getMessage());
                }
            }
        }//end of ListOfNumbers class


        try{
            //intansiate
            ListOfNumbers list = new ListOfNumbers();
            list.writeList();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("Finally block executed, no matter what!");
        }
        //example
        try{
            System.out.println(5 * 10);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("5 * 10 try-catch-finally block executed");
        }

    }//end of psvm



}
