package shapes;


//add abstract, then extends Shape, implements
    public abstract class Quadrilateral extends Shape implements Measurable {
        //fields
        protected double length;
        protected double width;

        //constructor
    public Quadrilateral(double length, double width) {
        this.length = length;
        this.width = width;

    }

    //methods for setting width and length


        public double getLength() {
            return length;
        }

        abstract void setLength(double length);//abstract dont have a body

        public double getWidth() {
            return width;
        }

        abstract void setWidth(double width);
    }
