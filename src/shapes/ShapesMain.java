package shapes;

public class ShapesMain {

    //variables


    public static void main(String[] args) {
//    Rectangle box1 = new Rectangle(5,4);// new object: Rectangle box1. new instance: new Rectangle
////        System.out.println(box1.getArea());



    Rectangle box1 = new Square(5);


    // Measurable myShape

    Measurable myShape = new Rectangle(5, 4);
    Measurable myShape1 = new Square(5);
    Measurable myShape3 = new Rectangle(7, 5);


        System.out.println(myShape.getArea());
        System.out.println(myShape.getPerimeter());
        System.out.println(myShape1.getArea());
        System.out.println(myShape1.getPerimeter());
        System.out.println(myShape3.getArea());
        System.out.println(myShape3.getPerimeter());







    }//end of Main Class


}//end of class Shapes
