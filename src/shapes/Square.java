package shapes;



//add extends Quad, and implement methods. create new constructor or activate old one

// in your
public class Square extends Rectangle {


//constructor

    //do shortcut for constructor to inherit rectangle. then change parameters.
    //this will allow us to use one parameter
    public Square(double side) {
        super(side,side);

    }//after this go to main method

    @Override
    public double getPerimeter() {
        return 4 * this.length;
//        return length * length;
    }

    @Override
    public double getArea() {
        return Math.pow(this.length, 2);
    }



    //override
//    @Override
//    //inherit variable length from the Rectangle class
//    public double getArea() {
//        return ;// returns 10
//    }
//    public double getPerimeter() {
//        System.out.println(4 * this.length);
//    }






}
