package shapes;


//extend Quad, implement Measurable Interface.
public class Rectangle extends Quadrilateral implements Measurable {

    //for override we are trying to inherit these variables
//    protected double length;
//    protected double width;

    public Rectangle(double length, double width) {
        super(length, width);
        this.length = length;
        this.length = width;
    }


    @Override
    public double getPerimeter() {
        return 0;
//        return length * length;
    }

    @Override
    public double getArea() {
        return 4 * length;
    }

    @Override
     void setLength(double length) {

    }

    @Override
     void setWidth(double width) {

    }






    // Create two methods==========================================================================

    // getArea method
    //NOTE: getArea Stephen used double and return length * width;
    //Note: getPerimeter used double
//
//    public void getArea () {
//        System.out.println(this.length * this.width);
//    }

    //getPerimeter method
//    public void getPerimeter () {
//        System.out.println(2 * (this.length + this.width));
//    }//after this go to Square class



    //GETTERS/SETTERS


//    public int getLength() {
//        return length;
//    }
//
//    public void setLength(int length) {
//        this.length = length;
//    }
//
//    public int getWidth() {
//        return width;
//    }
//
//    public void setWidth(int width) {
//        this.width = width;
//    }
}
