import java.util.ArrayList;
import java.util.Collections;

public class CollectionsArraysExercise {




    public static void main(String[] args) {

        //integers array
        ArrayList<Integer> myNumbers = new ArrayList<>();
        myNumbers.add(23);
        myNumbers.add(50);
        myNumbers.add(32);
        myNumbers.add(91);

        System.out.println(myNumbers);

        //strings array
        ArrayList<String> roasts = new ArrayList<>();
        roasts.add("light"); //similar to
        roasts.add("medium");
        roasts.add("dark");

        System.out.println(roasts);

        //array iteration
        ArrayList<String> usernames = new ArrayList<>();
        usernames.add("Alyssa");
        usernames.add("Angela");
        usernames.add("Jose");
        usernames.add("Stephen");
        usernames.add("Victor");

        //enhanced for loop
        for (String name : usernames) {
            System.out.println(name);
        }
        //regular for loop
        for (int i = 0; i< usernames.size(); i++) {
            System.out.println(usernames.get(i));
        }


        //insert elements into the array
        usernames.set(0, "Allen");
        usernames.set(4, "Tim");
        System.out.println(usernames);

        //remove the third element

                usernames.remove(2);
                System.out.println(usernames);


        //DOG BREEDS
        ArrayList<String> dogBreed = new ArrayList<>();
        dogBreed.add("German Shepherd");
        dogBreed.add("Bull Dog");
        dogBreed.add("Rottweiler");
        dogBreed.add("Doberman");

        //sort the array

        System.out.println(dogBreed);
        Collections.sort(dogBreed);
        System.out.println(dogBreed);


        //CAT BREEDS
        ArrayList<String> catBreed = new ArrayList<>();
        catBreed.add("Russian Blue");
        catBreed.add("Bombay");
        catBreed.add("Persian");
        catBreed.add("Maine Coon");

        // search for an element
        boolean result;
        result = catBreed.contains("Persian");
        System.out.println(result ? "It does contain Persian!" : "Does not contain Persian");
        //if /else solution

        //ternary operator
//        System.out.println(catBreed.contains("Bombay") ? "is in the list");

        //reverse the elements in the array
        Collections.reverse(catBreed);
        System.out.println(catBreed);









    }


}
