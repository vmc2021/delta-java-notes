import java.util.Arrays;

public class ArraysLesson2 {

    //import main method
    public static void main(String[] args) {
        //step 1:
        //declared people as an array
        String[] people = {"Ron", "Fred", "Sally"};
        System.out.println(Arrays.toString(people));//returns [Ron, Fred, Sally]

        //step 6:
        //created a morePeople array. assigned array to addPerson.
        //addPerson array got passed the  people array, and string value of "kevin"
        String[] morePeople = addPerson(people, "Kevin");
        //step 7:
        System.out.println(Arrays.toString(morePeople));// returns [Ron, Ferd, Sally, Kevin]

    }//end of psvm


    //step:2
    //method addPerson . return type strings. addPerson has two parameters
    public static  String[] addPerson(String[] peopleArray, String person){
        //step 3:
        //create a new array
        String[] peopleCopy = Arrays.copyOf(peopleArray, peopleArray.length + 1);

        //step 4:
        //now taking the index of peopleCopy.length- 1/last index. assigning peopleCopy to = person
        peopleCopy[peopleCopy.length - 1] = person;

        //step 5: return statement
        // peopleCopy {lastIndexOf] = person
        return peopleCopy;

        //run this in psvm


    }



}//end of class
