public class Person {

    private String name;




    //empty constructor
    public Person()  {

    }


    //constructor with one parameter
    public Person (String name) {
        this.name = name;
    }



// getter/ setters methods
    public String getName () {
        return this.name;

    }
// getter / setter methods
    public void setName(String name) {
         this.name = name;
    }


    public void sayHello() {
        System.out.println("Hello I am " + this.name);
    }


}
