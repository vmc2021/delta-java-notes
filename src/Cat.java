public class Cat {
    //class with an object of a cat
    //blueprint of what a cat is
    //instance variables
    String name;
    int age;
    String color;
    String breed;

    //instance methods
    public static void sleep() {
        System.out.println("This cat is sleeping..");
    }

    public static void play() {
        System.out.println("This cat is now playing..");
    }
    public static void eat() {
        System.out.println("This cat is now eating..");
    }
    public String greeting() {
        return String.format("Hello I an %s", name);
    }
    // are done typing this object
}
