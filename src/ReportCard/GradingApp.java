package ReportCard;

import java.util.HashMap;
import java.util.Scanner;

public class GradingApp {

    public static void main(String[] args) {
             //<key  /  value/student objects>  /
        HashMap<String, Student> students = new HashMap<>();

        //create 4 student objects with at least 3 grades
        students.put("student1", new Student("Angela"));
        students.put("student2", new Student("Alyssa"));
        students.put("student3", new Student("Victor"));
        students.put("student4", new Student("Jose"));

        students.get("student1").addGrade(90);// add 3 grades
        students.get("student1").addGrade(91);
        students.get("student1").addGrade(92);

        students.get("student2").addGrade(91);// add 3 grades
        students.get("student2").addGrade(92);
        students.get("student2").addGrade(93);

        students.get("student3").addGrade(92);
        students.get("student3").addGrade(93);
        students.get("student3").addGrade(94);

        students.get("student4").addGrade(93);
        students.get("student4").addGrade(94);
        students.get("student4").addGrade(95);
        while (true) {
            System.out.println("Here are the user names: student1, student2, student3, student4" );
            System.out.println("Which student to you want to information about?");
            Scanner scanner = new Scanner(System.in);
            String userName = scanner.next();

            if (students.containsKey(userName)) {
                Student s1 = students.get(userName);
                System.out.println("Here is the name and grade average: " + s1.getName() + " " + s1.getGradeAverage() );
            }else{
                System.out.println("User name invalid");
            }
            System.out.println("Do you want information on another student? [Y/N] ");
            String userInput = scanner.next();
            if(userInput.equals("N")){
                break;
            }
        }

        // loop through the list of usernames


    }






}
