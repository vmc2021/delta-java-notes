package ReportCard;

import java.util.ArrayList;
import java.util.Collections;

public class Student {


    //fields
    private String name;
    //array property
    private ArrayList<Integer> grades;


    //constructor

    public Student(String name) {
        this.name = name;
        //'this' is a reference to the the class 'Student'
        this.grades = new ArrayList<>();
    }


    //getters and setters

    //returns student's name
    public String getName() {
        return name;
    }

    //
    public void addGrade(int grade) {
        grades.add(grade);
    }

    //add all grades then divide
    public double getGradeAverage() {
        double total = 0;
        for (int i = 0; i < grades.size(); i++) {
            total += grades.get(i);// total = total + grades.get();

        }
        return total / grades.size();
    }
}








