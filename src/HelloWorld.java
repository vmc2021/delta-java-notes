public class HelloWorld {
    //main method is like ngOninit in angular. Use shortcut psvm press enter
    public static void main(String[] args) {


        // working inside main method

        //first lesson is sco
        System.out.println("Hello World");
        System.out.println("Welcome to Java");
        //multiline comments

        //strings. Need to be in double quotations ""
        // car - character needs to be in single quotes ''

        System.out.println("Escape characters: ");
        System.out.println("First\\"); // \
        System.out.println("Second\n"); // new line
        System.out.println("Third\t tab character"); // tab button

        //VARIABLES. all variables in java must be declared before being used
        //syntax: dataType nameOfVariable
        //we cant use int int= 2, integer, for,...and many more. Cant duplicate variables
        byte age= 13;
        short myShort= -32768;

        int myInteger;
        myInteger= 18;

        boolean isAdmin;
        isAdmin= false;
        System.out.println(isAdmin);

        //CASTING. TURNING ONE VALUE INTO ANOTHER
        // Two types: Implicit and Explicit casting

        //Implicit Example
        int myInt = 900;
        long morePrecise= myInt;
        System.out.println(morePrecise);

        //EXPLICIT EXAMPLE
        double pi = 3.14159;
        int almostPi = (int) pi;
        System.out.println(almostPi);


    }
}
