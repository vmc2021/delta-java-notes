import java.util.Arrays;

public class ArraysLesson {

    //add main method below

    public static void main(String[] args) {
        //syntax: type [] mameOfArray;

        int[] numbers;

        double[] prices;
        String[] names;

        String[] developers = new String[5];
        developers[0] = "Victor";
        developers[1] = "Angela";
        developers[2] = "Alyssa";
        developers[3] = "Jose";
        developers[4] = "Justin";

        System.out.println(developers[3]);//returns jose
        System.out.println(developers.length);//5

        /*
        Assign a var we created a new array where the size is determined by a constant
         */

        int[] nums = new int[3];
        nums[0] = 11;
        nums[1] = 12;
        nums[2] = 13;

//        nums[3] = 14;
//        System.out.println(nums[3]);// ArrayIndexOutOfBoundsException

        //ARRAYS IN JAVASCRIPT
        // var numbers = [1,2,3,4]

        //ARRAYS IN JAVA

        //version1
        String [] languages = {"html", "css", "javascript", "jquery", "angular", "java"};

        //version2
        // String[] languages = new String[6]
        // languages[0] = "html";

        System.out.println(languages[2]);// jquery
        System.out.println(languages.length);//returns 3

//========================================================================================================
        //iterating arrays


        //regular for loop
        //using our languages array...

        //version1
        for (int i = 0; i < languages.length; i++) {
//            System.out.println(languages[i]);
            System.out.println("language : " + languages[i]);
            System.out.println();
        }

        //enhanced for loop

        //version2
        for (String language : languages) {
            System.out.println("Enhanced for loop : " + language);
        }

//=============ARRAY CLASSES: .==================================================================

        String[] testArray= new String[4];

        //method from arrays class

        Arrays.fill(testArray,"Badgers");
        // .fill() fills all
//        for (String element : testArray) {
//            System.out.println(element);
//        }

        //  .toString()  -  prints out a copy of the array contents

        //used to see end result
//        System.out.println(Arrays.toString(testArray));


        // .equals() - returns true if two array elements are equal and
        // in the same order
        String[] words = {"hello", "goodbye"};
        String[] word2 = {"hello", "Goodbye"};
//        System.out.println(Arrays.equals(words, word2));//return false

//========================================================================================
        //    .copyOf() - returns a copy array of a given array of a given length

        String[] myArray =  new String[4];
        myArray[0] = "abby";
        myArray[1] = "brian";
        myArray[2] = "cathy";
        myArray[3] = "david";
        String[] myArray2 = Arrays.copyOf(myArray, myArray.length);// to get just the first two. Erase myArray.length and enter: 2
//        System.out.println(Arrays.toString(myArray));
//        //returns
//        System.out.println(Arrays.toString(myArray2));

     //======================================================================================
        // Arrays.sort(array, startIndex, toIndex)
        // sorts array elements alphabetically/numerically.
        // 2nd and 3rd are optional.


        String[] breakfast = {"eggs", "cereal", "milk", "pancakes"};
//        System.out.println(Arrays.toString(breakfast));
        String[] breakfastCopy = Arrays.copyOf(breakfast, breakfast.length);

        Arrays.sort(breakfastCopy);
//        System.out.println(breakfastCopy);
//        System.out.println(Arrays.toString(breakfastCopy));

        //============TWO DIMENSIONAL ARRAYS slide 6===================

        //NESTING ARRAYS TOGETHER. HAS ROWS AND COLUMNS

        //INITIALIZE
        int[][] matrix =
                //numbers inside two dimensional arrays become integers
                { {1 ,2 ,3  }, {4, 5, 6 }, { 7, 8, 9}   };
        System.out.println(Arrays.toString(matrix[0]));
        System.out.println(matrix[0][1]);// returns 2
        System.out.println(matrix[2][2]);//9
        System.out.println(matrix[0][0]);// 1
        System.out.println(matrix[1][2]);//6


        //=================Looping thru a matrix==============



        //first enhanced for loop. for each row

        for (int[] row : matrix) {
            System.out.println("+---+---+---+");// top line. produces rows
            System.out.print("| ");//use print doesn't create a new line. produces a column
            //second enhanced loop. looking for row and column
//            for (int c : row) {
//                System.out.print(c + " | ");//use print
//            }
//            System.out.println();
        }
        System.out.println("+---+---+---+");//bottom line






    }//end of main method





}//end of class
