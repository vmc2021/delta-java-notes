package validation;// package = directory

import java.util.Scanner;

public class input {

    private Scanner scanner;

    public static Boolean yesNo() {
    Scanner scanner = new Scanner(System.in);
    String answer = "yes";
        System.out.println("Would you like to enter a number? [yes/No]");
        answer = scanner.next();
        if (answer.equalsIgnoreCase("yes")) {
            return true;
        }
        else {
            return false;
        }
    }

    public static int getInt(int min, int max){
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Please enter a valid number (%d - %d)%n", min, max);
        int userInput = scanner.nextInt();
        if (userInput >= min && userInput <= max){
            System.out.println("Valid number!");
            System.out.println("You entered: " + userInput);
            return userInput;
        }
        else {
            System.out.println("Invalid entry: Number must be between " +min+ "-" +max);
            return getInt(min, max);
        }

    }
    public static double getDouble(double min, double max){
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Please enter a valid double (%f - %f)%n", min, max);
        double userInput = scanner.nextDouble();
        if (userInput >= min && userInput<= max){
            System.out.println("Valid double!");
            System.out.println("You entered: " + userInput);
            return userInput;
        }
        else{
            System.out.println("Invalid entry: Double must be between " +min+ "-" +max);
            return getDouble(min, max);
        }

    }











    public static void main(String[] args) {
        System.out.println(yesNo());
        System.out.println(getInt(1, 10));
        System.out.println(getDouble(1, 10));






    }



}
