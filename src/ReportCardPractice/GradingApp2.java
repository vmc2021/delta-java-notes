package ReportCardPractice;

import java.util.HashMap;
import java.util.Scanner;

public class GradingApp2 {

    //add PSVM
    public static void main(String[] args) {

        //ADD HASHMAP
        // KEY: String
        //VALUE: Student2
        //NAME: students2
        HashMap<String, Student2> students2 = new HashMap<>();
        //ADD 4 STUDENT OBJECTS
        students2.put("first", new Student2("Ricky"));
        students2.put("second", new Student2("Eddy"));
        students2.put("third", new Student2("Bill"));
        students2.put("fourth", new Student2("Ted"));

        students2.get("first").addGrade(90);
        students2.get("first").addGrade(90);
        students2.get("first").addGrade(90);

        students2.get("second").addGrade(91);
        students2.get("second").addGrade(91);
        students2.get("second").addGrade(91);

        students2.get("third").addGrade(92);
        students2.get("third").addGrade(92);
        students2.get("third").addGrade(92);

        students2.get("fourth").addGrade(93);
        students2.get("fourth").addGrade(93);
        students2.get("fourth").addGrade(93);

        System.out.println("Welcome to the student grade app!");
        System.out.println("Here are the gitlab usernames of all the students:\n ");


        do {

            Scanner scanner = new Scanner(System.in);
            // list of usernames
            for (String s: students2.keySet()) {
                System.out.printf("|%s|", s);
                System.out.println();
            }
            System.out.println("Which student do you want to see more information on? \n");
            String userInput = scanner.next();
            if (userInput.equalsIgnoreCase("first")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("first").getName());
                System.out.println(students2.get("first").getGradeAverage());

            }else if (userInput.equalsIgnoreCase("second")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("second").getName());
                System.out.println(students2.get("second").getGradeAverage());

            }else if (userInput.equalsIgnoreCase("third")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("third").getName());
                System.out.println(students2.get("third").getGradeAverage());

            }else if (userInput.equalsIgnoreCase("fourth")) {
                System.out.printf("Gitlab Username: %s", userInput);
                System.out.println("Name: ");
                System.out.println(students2.get("fourth").getName());
                System.out.println(students2.get("fourth").getGradeAverage());
            }


        }while (true);



    }

}
