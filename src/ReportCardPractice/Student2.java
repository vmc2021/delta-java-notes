package ReportCardPractice;

import java.util.ArrayList;

public class Student2 {

    //fields
    private String name;
    private ArrayList<Integer>grades;

    //constructor
    public Student2(String name) {
        this.name = name;
        this.grades = new ArrayList<>();
    }


    //methods
    public String getName(){
        return name;
    }
    public void addGrade(int grade) {
        grades.add(grade);
    }
    public double getGradeAverage(){
        int total = 0;
        for (int grade : this.grades){
            total += grade;
        }
        return (double) total/grades.size();
    }



    //for testing

    public static void main(String[] args) {
        Student2 s1 = new Student2("Ricky");

        s1.addGrade(90);
        s1.addGrade(98);
        s1.addGrade(100);
        System.out.println(s1.getGradeAverage());
    }

}
