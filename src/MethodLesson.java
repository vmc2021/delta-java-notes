public class MethodLesson {
    public static  void main(String[] args) {

        //lets add numbers
        int sum = addNumbers(2,3);
        System.out.println("Using our static method: ");
        System.out.println(sum);

        System.out.println(addNumbers(2, 3));

        //lets call our tenureMessage() method
        System.out.println(tenureMessage("Stephen", "Java", 3));
        System.out.println(tenureMessage("Angular"));


        sayHello();//returns Hello World!
        System.out.println();
        sayHello(5); // returns Hello World!Hello World!Hello World!Hello World!Hello World!
        System.out.println();
        sayHello("Delta"); // returns Hello Delta!
        System.out.println();
        sayHello("Howdy","Zion"); //Howdy Zion

    }//END of PSVN

    //method structure
    // public: is the visibility profile
    //static: defines that method belongs to the class
    //int: the return type of the method. it needs to return an integer(s): num1,num2
    //data type then the parameter. We can duplicate parameters
    //void methods can be static or non-static. main point is that they will NOT have to return anything
    public static int addNumbers(int num1, int num2){
        return num1 + num2;
    }
    public static int subtractNumbers(int num1, int num2){
        return num1 - num2;//return to use in application or sout to console log it
    }
    public void greetings() {
        System.out.println("Hello, good day!");
    }


    //USE SAME NAME OF METHOD BUT DIFFERENT PARAMETERS
    //we get error because we are lacking a return statement

    //version 1 tenureMessage
    public static String tenureMessage(String name, String progLang, int numYears) {
        return name + "has been coding " + progLang + " for " + numYears + " years";// to up to main method "Stephen"

        //return "x has been

    }
//method overloading=================================================

    //version 2 tenureMessage
public  static  String tenureMessage(String proLang) {
        return "Somebody is good at " + proLang; //to up to main method "Angular"
}



//version 1 sayHello
public static void sayHello(int times) {
        for (int i =0; i < times; i++) {
            sayHello();//calling sayHello
        }
}

//version 2 sayHello
public static  void  sayHello() {
        sayHello("Hello", "World");// no method calling sayHello(greeting,name)
}
//version 3 sayHello
public static void sayHello(String greeting, String name) {
    System.out.printf("%s %s!", greeting, name);
}

//vers 4 sayHello
    public static  void  sayHello(String name) {
        sayHello("Hello", name);//calling sayHello(greeting,name)
    }





}
