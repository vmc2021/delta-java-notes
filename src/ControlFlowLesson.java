import java.util.Locale;
import java.util.Scanner;

public class ControlFlowLesson {

    public static void main(String[] args) {

        //working inside main method
//
//        //comparison operators: ==, >, <, !=, <=, >=
//        System.out.println(5 != 2); //true
//        System.out.println(5 >= 5); //true
//        System.out.println(5 <= 5); //true
//
//
//        //logical operators:  &&, ||
//        System.out.println(5 == 6 && 2>1 && 3 != 3); //false. for true all must be true
//        System.out.println(5 != 6 || 1<2 || 3 != 3);// true

        //.equals(), .equalsIgnoreCase()


        Scanner scanner = new Scanner(System.in);
//        System.out.println("Would you like to start? [y/N]");
//        String userInput = scanner.nextLine();
        //logic goes here
//        boolean confirm = userInput == 'y';// avoid
//          boolean confirm = userInput.equals('y');// do this instead

//        if (userInput.equals("y")) {
//            System.out.println("Game has started, good luck");
//        }
//        else if(userInput.equalsIgnoreCase("n")) {
//            System.out.println("You chose to not start the game, good bye");
//        }
//        else {
//            System.out.println("Answer not recognized...");
//        }

        //if statement. if false add an else statement
//        char letter = ';';// single quotes for char. value is " :"
//        if (letter == 'y') {
//            System.out.println("The letter was y. I guess that means Yes.");
//        }
//        else if (letter == 'n') {
//            System.out.println("So you're saying no?");
//        }
//        else {
//            System.out.println("I dont know what that means..");
//        }

        //switch statement using cases not conditions(if,else if, else)
        System.out.println("Enter a grade");
        String userInput = scanner.nextLine().toUpperCase();

        switch (userInput) {
            case "A":
                System.out.println("Distinction Honors");
                break;
            case "B":
                System.out.println("Grade: B");
                break;
            case "C":
                System.out.println("Grade: C");
                break;
            case "D":
                System.out.println("Grade: D");
                break;
            case "F":
                System.out.println("Fail..");
                break;
            default:
                System.out.println("Invalid entry");


        }

        //while loop
//        int i = 0;
//        while (i <= 10) {
//            System.out.println("i is " + i);
//            i++;
//        }
//
        //do while loop
//        int x = 0;
//        do{
//            System.out.println(x);
//            x++;
//        } while (x < 5);

        //for loop. intitialization, condition, incrimentation
//        for(int y =0; y < 10; y++) {
//            System.out.println(y);
//        }

        //break and continue
//        for(int a = 10; a <= 100; a++) {
//
//            System.out.println(a);
//
//            if (a == 21) {
//                break;
//            }
//
//        }
//        for(int a = 10; a <= 100; a++) {
//
//            if (a == 21) {
//                continue;
//            }
//            if (a == 89) {
//                break;
//            }
//            System.out.println(a);
//        }



    }


}

